Miscellaneous objects and functions
===================================

.. autoclass:: gpaw.lfc.LocalizedFunctionsCollection
    :members:

.. autoclass:: gpaw.spline.Spline
    :members:

.. autoclass:: gpaw.poisson.FDPoissonSolver
    :members:

.. autoclass:: gpaw.xc.functional.XCFunctional
    :members:

.. autoclass:: gpaw.xc.gga.GGA
    :members:

.. autofunction:: gpaw.forces.calculate_forces

.. autoclass:: gpaw.grid_descriptor.GridDescriptor
    :members:

.. autoclass:: gpaw.scf.SCFLoop
    :members:

.. autoclass:: gpaw.band_descriptor.BandDescriptor
    :members:
